(function() {
  'use strict';
  angular
      .module('App', ['ui.router','ui-notification', 'platanus.rut'])
      .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
          $locationProvider.hashPrefix('');
          $urlRouterProvider.otherwise('/');
          $stateProvider
              .state('home', {
                  url: '/',
                  templateUrl: 'app/views/home.html',
                  controller: 'HomeController'
              })
              ;
      }).run(function ($rootScope,$state) {
      $rootScope.$state = $state;
  });
})();
