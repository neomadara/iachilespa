(function() {
  'use strict';
  angular
      .module('App')
      .factory('contactServices', contactServices);

      contactServices.$inyect = ['$http','$q','setting'];

  function contactServices($http,$q,setting) {
      var service = {
          addContact:addContact
      };

      function addContact(obj) {
          var url = setting.url + 'landing/subscriptions';
          var deferred = $q.defer();
          var service = {
              name:obj['name'],
              tel:obj['phone'],
              email:obj['email'],
              rut:obj['rut']
          };

          $http({
              method: 'POST',
              url: url,
              data: $.param(service),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).then(function (response) {
              deferred.resolve(response.data)
          });
          return deferred.promise;
      }

      return service;

  }
})();
