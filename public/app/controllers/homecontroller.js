(function () {
    'use strict';
    angular
        .module('App')
        .controller('HomeController', HomeController);
        HomeController.$inyect = ['$scope','Notification','contactServices'];

    function HomeController($scope, Notification, contactServices) {
        $scope.form_data = [];

        $scope.save_data = function(form){

          var txt_fields = '';

          if(form.name == ""){
            txt_fields = txt_fields + "*Nombre" + "<br>"
          }
          if(form.phone == ""){
            txt_fields = txt_fields + "*Teléfono" + "<br>"
          }
          if(form.email == ""){
            txt_fields = txt_fields + "*Correo" + "<br>"
          }
          if(form.rut == ""){
            txt_fields = txt_fields + "*Rut" + "<br>"
          }

          if(txt_fields!=""){
            Notification.info({message: '<strong>Debe ingresar</strong>: <br> ' + txt_fields, title: '<strong>Información</strong>'});
          } else {

            form.rut = $("#rut").val();
            contactServices.addContact(form).then(function(res){
              $scope.form_data = [];
              if(res.success) return Notification.success({message: 'Contacto creado exitosamente', title: 'Información'})
              return Notification.success({message: 'El Contacto no pude ser creado', title: 'Información'})
            })

          }

        }

    }

})();
