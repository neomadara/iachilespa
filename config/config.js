const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'iachilespa'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/iachilespa-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'iachilespa'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/iachilespa-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'iachilespa'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/iachilespa-production'
  }
};

module.exports = config[env];
