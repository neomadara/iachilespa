const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Contact = mongoose.model('Contact');

module.exports = (app) => {
  app.use('/', router);
};

router.get('/', (req, res, next) => {
  res.render('index');
});

router.post('/landing/subscriptions', function(req, res){
  var newContact = new Contact({
    name: req.body.name,
    tel: req.body.tel,
    email: req.body.email,
    rut: req.body.rut
  });
  newContact.save(function(err){
    if (err) return res.json({ message: err, success: false });
    return res.json({ message: '', success: true })
  });
})
