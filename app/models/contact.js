const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContactSchema = new Schema({
  name: String,
  tel: String,
  email: String,
  rut: String
});

ContactSchema.virtual('date')
  .get(() => this._id.getTimestamp());

mongoose.model('Contact', ContactSchema);

